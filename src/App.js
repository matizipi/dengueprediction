import './App.css';
import { HomeIndex } from './modules/home';

function App() {
  return (
    <div className="App"> 
     <HomeIndex.Home/>
    </div>
  );
}

export default App;
