import React, {useState} from 'react'
import "./Home.css"
import { provinciasArgentina } from './provincias';

const Home = () => {
  const [provincia, setProvincia] = useState(0);
  const [edad, setEdad] = useState("");
  const [edadId, setEdadId] = useState(0);
  const [porcentaje,setPorcentaje] = useState(null);
  const [cantCasos,setCantCasos] = useState(0);

  const handleEdadId = (value) => {
    switch (true) {
      case value <= 1:
        return 2;
      case value <= 4:
        return 3;
      case value <= 9:
        return 4;
      case value <= 14:
        return 5;
      case value <= 19:
        return 6;
      case value <= 24:
        return 7;
      case value <= 34:
        return 8;
      case value <= 44:
        return 9;
      case value <= 65:
        return 10;
      default:
        return 11;
    }
  }
  
  const handleChange = (event) => {
    const { name, value } = event.target;
    if (name === 'provincia') {
      setProvincia(value);
    } else if (name === 'edad') {
      setEdad(value);
      setEdadId(handleEdadId(value));
    }
  }

  const handleCantCasos = () => {
    if(cantCasos >= 500)
     return true;
    return false;
  }

  const handleDisabled = () => {
    if(edadId !== 0 && provincia !== 0)
     return false
    return true
  }

  const handlePorcentaje = (porcentaje) => {
    if (porcentaje <= 15) {
      return "Poco probable";
    } else if (porcentaje <= 50) {
      return "Estar Atento";
    } else {
      return "Muy probable";
    }
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
        const url = `https://matizipis.pythonanywhere.com/?provincia=${provincia}&grupo=${edadId}`;

        const headers = {
            'Content-Type': 'application/json', 
          };
      
          const response = await fetch(url, {
            method: 'GET', 
            headers: headers, 
          });
  
        if (response.ok) {
          const data = await response.json();

          setPorcentaje(data.porcentaje);
          setCantCasos(data.cantidad_casos);
  
          console.log('Respuesta del servidor:', data);
        } else {
          console.error('La solicitud no tuvo éxito. Código de estado:', response.status);
        }
      } catch (error) {
        console.error('Error al hacer la petición:', error);
      }
  }

  return (
    <div>
      <h1>Dengue Calculator</h1>
      <form onSubmit={handleSubmit} className='form'>
        <div className='divContainer'>
          <label htmlFor="provincia" className='label'>Provincia</label>
          <select
            className='input select'
            id="provincia"
            name="provincia"
            value={provincia}
            onChange={handleChange}
          >
            <option value="" key={0}>Selecciona una provincia</option>
            {provinciasArgentina.map((prov) => (
              <option key={prov.id} value={prov.id}>
                {prov.nombre}
              </option>
            ))}
          </select>
        </div>
        <div className='divContainer'>
          <label htmlFor="edad" className='label'>Edad</label>
          <input
            className='input'
            id="edad"
            name="edad"
            value={edad}
            onChange={handleChange}
          />
        </div>
        <button type="submit" className='button' disabled={handleDisabled()}>Calcular</button>
      </form>
     {porcentaje && <div className='responseContainer'>
        <label className='label response'>En base a los datos obtenidos el resultado es <br/> {handlePorcentaje(porcentaje)}</label>
        {handleCantCasos() && <>
        <br/>
        <br/>
        <label className='label response'>Esta provincia tiene bastantes casos de dengue <br/> tener precaución</label>
        </>
        }
      </div>}
      <a href="https://www.argentina.gob.ar/salud/glosario/dengue#:~:text=%C2%BFC%C3%B3mo%20puede%20prevenirse%3F,casas%20como%20en%20sus%20alrededores." target="_blank" rel="noopener noreferrer">
        Como prevenir el Dengue?
      </a>
    </div>
  );
}

export default Home;
